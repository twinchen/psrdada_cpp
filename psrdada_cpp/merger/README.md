# Merger
The merger module aligns polarisation or frequency data to a expected format for pulsar pipelines (e.g. `dspsr`)

## Input Data Format

- Packetizer both polarizations
- Alveo PFB

## Output Data Format

...

## Modes
- PFB / frequency
- Polarisation