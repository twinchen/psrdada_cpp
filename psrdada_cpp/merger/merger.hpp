#pragma once
#include <cstring>
#include <vector>
#include <stdexcept>
#include <immintrin.h>
#include <time.h>
#include <iomanip>
#include <cmath>
#include <ascii_header.h>

namespace psrdada_cpp {
namespace merger {

struct merge_conf{
    std::size_t npol;
    std::size_t nchunk;
    std::size_t nthreads;
    std::size_t heap_size;
    std::size_t hgroup_size(){
        return heap_size * nchunk;
    }
};

class PFBMerger
{
public:
    PFBMerger(std::size_t nchunk, std::size_t nthreads, std::size_t heap_size);
    PFBMerger(merge_conf conf);

    ~PFBMerger();

    void process(char* idata, char* odata, std::size_t size);
private:
    std::size_t _nchunk;
    std::size_t _nthreads;
    std::size_t _heap_size;
};

class PolnMerger
{
public:
    PolnMerger(std::size_t npol, std::size_t nthreads);
    PolnMerger(merge_conf conf);

    ~PolnMerger();

    void process(char* idata, char* odata, std::size_t size);
private:
    std::size_t _npol;
    std::size_t _nthreads;
};

}
}