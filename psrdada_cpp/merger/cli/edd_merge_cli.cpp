#include <string>
#include <boost/program_options.hpp>

#include "psrdada_cpp/multilog.hpp"
#include "psrdada_cpp/cli_utils.hpp"
#include "psrdada_cpp/common.hpp"
#include "psrdada_cpp/dada_input_stream.hpp"
#include "psrdada_cpp/dada_write_client.hpp"
#include "psrdada_cpp/merger/merger.hpp"
#include "psrdada_cpp/merger/pipeline.hpp"


using namespace psrdada_cpp;

namespace
{
const size_t ERROR_IN_COMMAND_LINE = 1;
const size_t SUCCESS = 0;
const size_t ERROR_UNHANDLED_EXCEPTION = 2;
} // namespace

int main(int argc, char** argv)
{
    try
    {
        key_t input_key;
        key_t output_key;
        std::size_t npol;
        std::size_t nchunk;
        std::size_t nthreads;
        std::size_t heap_size;
        std::string mtype;

        /** Define and parse the program options
        */
        namespace po = boost::program_options;
        po::options_description desc("Options");
        desc.add_options()
        ("help,h", "Print help messages")
        ("input_key,i", po::value<std::string>()
         ->default_value("dada")
         ->notifier([&input_key](std::string in)
        {
            input_key = string_to_key(in);
        }),
        "The shared memory key for the dada buffer to connect to (hex string)")
        ("output_key,o", po::value<std::string>()
         ->default_value("dadc")
         ->notifier([&output_key](std::string out)
        {
            output_key = string_to_key(out);
        }),
        "The shared memory key for the dada buffer to connect to (hex string)")
        ("nchunck,c", po::value<std::size_t>(&nchunk)->default_value(2),
         "number of incoming stream")
        ("nthreads,n", po::value<std::size_t>(&nthreads)->default_value(4),
         "Value of number of threads")
        ("heap_size,b", po::value<std::size_t>(&heap_size)->default_value(8000), "Size of a heap")
        ("npol,p", po::value<std::size_t>(&npol)->default_value(2), "Size of a heap")
        ("merge_type,t", po::value<std::string>(&mtype)->default_value("pfb"), "The merge type, either 'pfb' or 'pol'")
        ("log_level", po::value<std::string>()
         ->default_value("info")
         ->notifier([](std::string level)
        {
            set_log_level(level);
        }),
        "The logging level to use (debug, info, warning, error)");

        po::variables_map vm;
        try
        {
            po::store(po::parse_command_line(argc, argv, desc), vm);
            if ( vm.count("help")  )
            {
                std::cout << "EDD Merger -- Read EDD data from a DADA buffer and merge it"
                          << std::endl << desc << std::endl;
                return SUCCESS;
            }
            po::notify(vm);
        }
        catch (po::error& e)
        {
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
            std::cerr << desc << std::endl;
            return ERROR_IN_COMMAND_LINE;
        }
        /**
         * All the application code goes here
         */
        MultiLog log("merger");
        DadaWriteClient output(output_key, log);
        if(mtype == "pfb"){
            merger::PFBMerger merger(nchunk, nthreads, heap_size);
            merger::Pipeline<decltype(merger), decltype(output)> pipeline(merger, output);
            DadaInputStream <decltype(pipeline)> input(input_key, log, pipeline);
            input.start();
        }else{
            merger::PolnMerger merger(npol, nthreads);
            merger::Pipeline<decltype(merger), decltype(output)> pipeline(merger, output);
            DadaInputStream <decltype(pipeline)> input(input_key, log, pipeline);
            input.start();
        }
        /**
         * End of application code
         */
    }
    catch (std::exception& e)
    {
        std::cerr << "Unhandled Exception reached the top of main: "
                  << e.what() << ", application will now exit" << std::endl;
        return ERROR_UNHANDLED_EXCEPTION;
    }
    return SUCCESS;

}
