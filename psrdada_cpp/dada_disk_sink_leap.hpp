#ifndef PSRDADA_CPP_EFFELSBERG_EDD_DADA_DISK_SINK_LEAP_HPP
#define PSRDADA_CPP_EFFELSBERG_EDD_DADA_DISK_SINK_LEAP_HPP

#include <chrono>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>

#include <ascii_header.h>

#include "psrdada_cpp/raw_bytes.hpp"
#include "psrdada_cpp/common.hpp"
#include "psrdada_cpp/dada_write_client.hpp"

#define HEADER_SIZE 4096
#define START_TIME 1024
#define HEAP_SIZE 32000

namespace psrdada_cpp {

class DiskSinkLeap
{
public:
    DiskSinkLeap(std::string prefix, int nchan);
    ~DiskSinkLeap();
    void init(RawBytes& block);
    bool operator()(RawBytes& block);

private:
    std::string _prefix;
    std::size_t _counter;
    std::vector<std::ofstream> _output_streams;
    size_t _nchan;
    char _header[HEADER_SIZE];
    char _start_time[START_TIME];
    bool first_block;
    std::vector<char> _transpose;
};

} //namespace psrdada_cpp

#endif //PSRDADA_CPP_EFFELSBERG_EDD_DADA_DISK_SINK_LEAP_HPP
