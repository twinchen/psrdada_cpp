if(ENABLE_CUDA)

set(PSRDADA_CPP_GS_LIBRARIES
    ${CMAKE_PROJECT_NAME}
    ${CMAKE_PROJECT_NAME}_common
    ${CMAKE_PROJECT_NAME}_gated_spectrometer
    ${CUDA_CUFFT_LIBRARIES}
    ${DEPENDENCY_LIBRARIES}
    ${PSRDADA_CPP_LIBRARIES}
)

set(psrdada_cpp_gated_spectrometer_src
    src/io.cu
    src/kernels.cu
    pipeline.cuh
)

set(psrdada_cpp_gated_spectrometer_inc
    io.cuh
    kernels.cuh
    pipeline.cuh
)

cuda_add_library(${CMAKE_PROJECT_NAME}_gated_spectrometer ${psrdada_cpp_gated_spectrometer_src})



install(FILES ${psrdada_cpp_common_inc} DESTINATION include/psrdada_cpp/gated_spectrometer)
install(DIRECTORY src DESTINATION include/psrdada_cpp/gated_spectrometer)
install (TARGETS ${CMAKE_PROJECT_NAME}_gated_spectrometer
    RUNTIME DESTINATION bin
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib
)

cuda_add_executable(gated_spectrometer cli/gated_spectrometer_cli.cu)
MESSAGE(STATUS  "XX ${PSRDADA_CPP_GS_LIBRARIES} ${PSRDADA_CPP_LIBRARIES}")
target_link_libraries(gated_spectrometer ${PSRDADA_CPP_GS_LIBRARIES})
install(TARGETS gated_spectrometer DESTINATION bin)

if (ENABLE_TESTING)
  add_subdirectory(test)
endif()
if (ENABLE_BENCHMARK)
    add_subdirectory(benchmark)
endif()

endif(ENABLE_CUDA)
