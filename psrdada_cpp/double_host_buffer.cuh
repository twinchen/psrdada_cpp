#ifndef PSRDADA_CPP_DOUBLE_HOST_BUFFER_HPP
#define PSRDADA_CPP_DOUBLE_HOST_BUFFER_HPP

#include "psrdada_cpp/double_buffer.cuh"
#include "thrust/host_vector.h"

#if CUDA_VERSION < 12000

#include "thrust/system/cuda/experimental/pinned_allocator.h"
namespace psrdada_cpp {

template <typename T>
using DoubleHostBuffer = DoubleBuffer<thrust::host_vector<T>>;
template <typename T>
using DoublePinnedHostBuffer = DoubleBuffer<thrust::host_vector<T, thrust::system::cuda::experimental::pinned_allocator<T>>>;

}

#else

#include "thrust/system/cuda/memory_resource.h"
template <typename T>
using mr = thrust::system::cuda::universal_host_pinned_memory_resource;
template <typename T>
using pinned_allocator = thrust::mr::stateless_resource_allocator<T, mr<T>>;

namespace psrdada_cpp {
template <typename T>
using DoubleHostBuffer = DoubleBuffer<thrust::host_vector<T>>;
template <typename T>
using DoublePinnedHostBuffer = DoubleBuffer<thrust::host_vector<T, pinned_allocator<T>>>;

}

#endif


#endif //PSRDADA_CPP_DOUBLE_HOST_BUFFER_HPP
