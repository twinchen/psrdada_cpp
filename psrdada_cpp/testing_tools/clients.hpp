#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "psrdada_cpp/dada_client_base.hpp"
#include "psrdada_cpp/multilog.hpp"
#include "psrdada_cpp/raw_bytes.hpp"

namespace psrdada_cpp {
namespace testing_tools {


class MockDadaClientBase{
public:
    virtual ~MockDadaClientBase(){}
    MOCK_METHOD(std::size_t, data_buffer_size, (), (const));
    MOCK_METHOD(std::size_t, header_buffer_size, (), (const));
    MOCK_METHOD(std::size_t, data_buffer_count, (), (const));
    MOCK_METHOD(std::size_t, header_buffer_count, (), (const));
    MOCK_METHOD(std::string, id, (), (const));
    MOCK_METHOD(void, connect, ());
    MOCK_METHOD(void, disconnect, ());
    MOCK_METHOD(void, reconnect, ());
    MOCK_METHOD(void, cuda_register_memory, ());
};

class MockDadaWriteClient : public MockDadaClientBase{

public:
    MockDadaWriteClient()
        : _header_stream(*this), _data_stream(*this) {}
    MockDadaWriteClient(key_t key, MultiLog& log)
        : _header_stream(*this), _data_stream(*this) {}
    MockDadaWriteClient(MockDadaWriteClient const&) = delete;

    class MockHeaderStream
    {
    public:
        MockHeaderStream(MockDadaWriteClient& parent)
            : _parent(parent) {}
        MOCK_METHOD(RawBytes&, next, ());
        MOCK_METHOD(void, release, ());
        MOCK_METHOD(std::size_t, block_idx, ());
    private:
        MockDadaWriteClient& _parent;
    };
    class MockDataStream
    {
    public:
        MockDataStream(MockDadaWriteClient& parent)
            : _parent(parent) {}
        MOCK_METHOD(RawBytes&, next, ());
        MOCK_METHOD(void, release, ());
        MOCK_METHOD(std::size_t, block_idx, ());
    private:
        MockDadaWriteClient& _parent;
    };

    MockHeaderStream& header_stream(){return _header_stream;}
    MockDataStream& data_stream(){return _data_stream;}

private:
    MockHeaderStream _header_stream;
    MockDataStream _data_stream;
};

}
}