#pragma once

#include <thrust/host_vector.h>
#include <gtest/gtest.h>
#include <vector>

#include "psrdada_cpp/fft_spectrometer/pipeline.cuh"
#include "psrdada_cpp/dada_null_sink.hpp"
#include "psrdada_cpp/double_host_buffer.cuh"
#include "psrdada_cpp/raw_bytes.hpp"
#include "psrdada_cpp/cuda_utils.hpp"
#include "psrdada_cpp/dada_db.hpp"

namespace psrdada_cpp {
namespace fft_spectrometer {
namespace test {

struct test_config{
    std::size_t fft_length;
    std::size_t tscrunch;
    std::size_t nsamps_out;
    std::size_t nbits;
};

class PipelineTester: public ::testing::TestWithParam<test_config>
{
protected:
    void SetUp() override;
    void TearDown() override;

public:
    PipelineTester();
    ~PipelineTester();

    void performance_test();

private:
    test_config _config;

};

} //namespace test
} //namespace fft_spectrometer
} //namespace psrdada_cpp
