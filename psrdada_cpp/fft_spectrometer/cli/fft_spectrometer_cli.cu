#include <time.h>
#include <ctime>
#include <boost/program_options.hpp>

#include "psrdada_cpp/multilog.hpp"
#include "psrdada_cpp/cli_utils.hpp"
#include "psrdada_cpp/common.hpp"
#include "psrdada_cpp/dada_client_base.hpp"
#include "psrdada_cpp/dada_input_stream.hpp"
#include "psrdada_cpp/dada_output_stream.hpp"
#include "psrdada_cpp/simple_file_writer.hpp"
#include "psrdada_cpp/dada_null_sink.hpp"
#include "psrdada_cpp/fft_spectrometer/pipeline.cuh"

using namespace psrdada_cpp;

namespace
{
  const size_t ERROR_IN_COMMAND_LINE = 1;
  const size_t SUCCESS = 0;
  const size_t ERROR_UNHANDLED_EXCEPTION = 2;
} // namespace


int main(int argc, char** argv)
{
    try
    {
        key_t input_key;

        int fft_length;
        int naccumulate;
        int nbits;

        std::time_t now = std::time(NULL);
        std::tm * ptm = std::localtime(&now);
        char default_filename[32];
        std::strftime(default_filename, 32, "%Y-%m-%d-%H:%M:%S.bp", ptm);
        std::string filename(default_filename);
        std::string output_type;

        /** Define and parse the program options
        */
        namespace po = boost::program_options;
        po::options_description desc("Options");
        desc.add_options()
        ("help,h", "Print help messages");

        desc.add_options()("input_key,i", po::value<std::string>()
            ->default_value("dada")
            ->notifier([&input_key](std::string in)
                {
                    input_key = string_to_key(in);
                }),
           "The shared memory key for the dada buffer to connect to (hex string)");

        desc.add_options()
        ("fft_length,n", po::value<int>(&fft_length)->required(),
            "The length of the FFT to perform on the data");

        desc.add_options()
        ("naccumulate,a", po::value<int>(&naccumulate)->required(),
            "The number of samples to integrate in each channel");

        desc.add_options()
        ("nbits,b", po::value<int>(&nbits)->required(),
            "The number of bits per sample in the packetiser output (8, 10, or 12)");

        desc.add_options()(
            "output_type", po::value<std::string>(&output_type)->default_value("file"),
            "output type [dada, file, profile]. Default is file. Profile executes the "
            " spectrometer 10x on random data and passes the ouput to a null sink."
        );
        desc.add_options()(
            "output_key,o", po::value<std::string>(&filename)->default_value(default_filename),
            "The key of the output bnuffer / name of the output file to write spectra "
            "to");

        desc.add_options()
        ("log_level", po::value<std::string>()
            ->default_value("info")
            ->notifier([](std::string level)
                {
                    set_log_level(level);
                }),
            "The logging level to use (debug, info, warning, error)");

        po::variables_map vm;
        try
        {
            po::store(po::parse_command_line(argc, argv, desc), vm);
            if ( vm.count("help")  )
            {
                std::cout << "FftSpectrometer -- Read EDD data from a DADA buffer and perform a simple FFT spectrometer"
                << std::endl << desc << std::endl;
                return SUCCESS;
            }
            po::notify(vm);
            if (vm.count("output_type") && (!(output_type == "dada" || output_type == "file" || output_type== "profile") ))
            {
              throw po::validation_error(po::validation_error::invalid_option_value, "output_type", output_type);
            }

        }
        catch(po::error& e)
        {
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
            std::cerr << desc << std::endl;
            return ERROR_IN_COMMAND_LINE;
        }
        /**
         * All the application code goes here
         */
        MultiLog log("FFT Spectrometer");
        DadaClientBase client(input_key, log);
        std::size_t buffer_bytes = client.data_buffer_size();


        if (output_type == "file"){

            SimpleFileWriter sink(filename);
            //NullSink sink;
            fft_spectrometer::Pipeline<decltype(sink)> spectrometer(buffer_bytes, fft_length, naccumulate, nbits, sink);

            DadaInputStream<decltype(spectrometer)> istream(input_key, log, spectrometer);
            istream.start();
        } else if (output_type == "dada"){
            DadaOutputStream sink(string_to_key(filename), log);

            fft_spectrometer::Pipeline<decltype(sink)> spectrometer(buffer_bytes, fft_length, naccumulate, nbits, sink);

            DadaInputStream<decltype(spectrometer)> istream(input_key, log, spectrometer);
            istream.start();
        } else if (output_type == "profile") {

            NullSink sink;
            fft_spectrometer::Pipeline<decltype(sink)> spectrometer(buffer_bytes, fft_length, naccumulate, nbits, sink);
            std::vector<char> buffer(fft_length * naccumulate * nbits / 8);
            cudaHostRegister(buffer.data(), buffer.size(), cudaHostRegisterPortable);
            RawBytes ib(buffer.data(), buffer.size(), buffer.size());
            spectrometer.init(ib);
            for (int i =0; i< 10; i++)
            {
              std::cout << "Profile Block: "<< i +1 << std::endl;
              spectrometer(ib);
            }

            DadaInputStream<decltype(spectrometer)> istream(input_key, log, spectrometer);
            istream.start();
         }

        /**
         * End of application code
         */
    }
    catch(std::exception& e)
    {
        std::cerr << "Unhandled Exception reached the top of main: "
        << e.what() << ", application will now exit" << std::endl;
        return ERROR_UNHANDLED_EXCEPTION;
    }
    return SUCCESS;

}
