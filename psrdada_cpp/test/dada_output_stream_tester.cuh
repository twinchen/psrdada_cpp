#pragma once

#include <gtest/gtest.h>
#include <cuda.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

#include "psrdada_cpp/multilog.hpp"
#include "psrdada_cpp/cli_utils.hpp"
#include "psrdada_cpp/cuda_utils.hpp"
#include "psrdada_cpp/dada_db.hpp"
#include "psrdada_cpp/dada_write_client.hpp"
#include "psrdada_cpp/dada_read_client.hpp"
#include "psrdada_cpp/dada_output_stream.hpp"
#include "psrdada_cpp/testing_tools/tools.cuh"

namespace psr_testing = psrdada_cpp::testing_tools;

namespace psrdada_cpp {
namespace test {


class StreamCopyFunctor : public AbstractCopyEngine
{
public:
    StreamCopyFunctor(cudaStream_t& stream)
    : _stream(stream){}

    void operator()(void* dst, void* src, std::size_t nbytes)
    {
        CUDA_ERROR_CHECK(cudaMemcpyAsync(dst, src, nbytes, cudaMemcpyDeviceToHost,_stream));
    }

    void sync()
    {
        CUDA_ERROR_CHECK(cudaStreamSynchronize(_stream));
    }
private:
    cudaStream_t _stream;
};

class DadaOutputStreamTester : public ::testing::Test
{
public:
    DadaOutputStreamTester(){}

    ~DadaOutputStreamTester(){}

    void SetUp() override;

    void TearDown() override;

    void test_operator_function();

private:
    StreamCopyFunctor* copy_engine;
    cudaStream_t stream;
    DadaReadClient* reader;
    DadaOutputStream* test_object;
    psrdada_cpp::DadaDB buffer;
};


}
}