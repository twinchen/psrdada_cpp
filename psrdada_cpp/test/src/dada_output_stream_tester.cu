#include "psrdada_cpp/test/dada_output_stream_tester.cuh"

namespace psrdada_cpp {
namespace test{


void DadaOutputStreamTester::SetUp()
{
    psrdada_cpp::MultiLog log("DadaOutputStreamTester");
    key_t  key = string_to_key("dada");
    buffer.create();
    CUDA_ERROR_CHECK(cudaStreamCreate(&stream));
    copy_engine = new StreamCopyFunctor(stream);
    reader = new DadaReadClient(buffer.key(), log);
    test_object = new DadaOutputStream(buffer.key(), log);
}

void DadaOutputStreamTester::TearDown()
{
    CUDA_ERROR_CHECK(cudaStreamDestroy(stream));
    delete test_object;
    delete reader;
    buffer.destroy();
}

void DadaOutputStreamTester::test_operator_function()
{
    char* actual;
    std::size_t nbytes = buffer.data_buffer_size();
    thrust::host_vector<char> expected1 = psr_testing::random_vector<char>(0, 64, nbytes);
    thrust::host_vector<char> expected2 = psr_testing::random_vector<char>(0, 64, nbytes);
    thrust::device_vector<char> device1 = expected1;
    thrust::device_vector<char> device2 = expected1;
    RawBytes raw1(reinterpret_cast<char*>(thrust::raw_pointer_cast(device1.data())), nbytes, nbytes, true);
    RawBytes raw2(reinterpret_cast<char*>(thrust::raw_pointer_cast(device1.data())), nbytes, nbytes, true);
    test_object->operator()(raw1, *(copy_engine));
    test_object->operator()(raw2, *(copy_engine));
    actual = reader->data_stream().next().ptr();
    for(std::size_t i = 0; i < nbytes; i++){
        ASSERT_EQ(expected1[i], actual[i]);
    }
    reader->data_stream().release();
    test_object->operator()(raw1, *(copy_engine));
    actual = reader->data_stream().next().ptr();
    for(std::size_t i = 0; i < nbytes; i++){
        ASSERT_EQ(expected2[i], actual[i]);
    }
    reader->data_stream().release();
}

TEST_F(DadaOutputStreamTester, test_operator_function)
{
    this->test_operator_function();
}

}
}
