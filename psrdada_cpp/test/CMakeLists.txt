include_directories(${GTEST_INCLUDE_DIR})

link_directories(${GTEST_LIBRARY_DIR})

set(gtest_base_cpp
    src/dada_output_stream_tester.cu
    src/gtest_base.cpp
)

cuda_add_executable(gtest_base ${gtest_base_cpp} )
add_dependencies(gtest_base googletest)
target_link_libraries(gtest_base ${PSRDADA_CPP_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_base gtest_base --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
