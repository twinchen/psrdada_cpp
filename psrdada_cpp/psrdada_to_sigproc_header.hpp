#ifndef PSRDADA_CPP_PSRDADA_TO_SIGPROC_HEADER_HPP
#define PSRDADA_CPP_PSRDADA_TO_SIGPROC_HEADER_HPP

#include "psrdada_cpp/raw_bytes.hpp"
#include "psrdada_cpp/common.hpp"

namespace psrdada_cpp {


template <class HandlerType>
class PsrDadaToSigprocHeader
{

public:
    PsrDadaToSigprocHeader(HandlerType& handler)
    : _handler(handler){}

    ~PsrDadaToSigprocHeader(){}

    /**
     * @brief      A header manipulation method for PSRDADA and SIGPROC
     *
     *
     * @details     A class that converts the PSRDADA header to a SIGPROC
     * 	           header before writing it out to the header block of the
     * 	           DADA buffer. This conversion is needed so that the down
     * 	           stream pipeline can handle the header format.
     *
     * @param      block  A RawBytes object wrapping a DADA header buffer
     */
    void init(RawBytes& block)
    {
	    SigprocHeader h;
        PsrDadaHeader ph;
        ph.from_bytes(block);
	    std::memset(block.ptr(), 0, block.total_bytes());
        h.write_header(block,ph);
        block.used_bytes(block.total_bytes());
        _handler.init(block);
    }


    /**
     * @brief      A callback to be called on acqusition of a new
     *             data block.
     *
     * @param      block  A RawBytes object wrapping a DADA data buffer
     */
    bool operator()(RawBytes& block)
    {
    	_handler(block);
        return false;
    }



private:
    HandlerType _handler;

};


} //psrdada_cpp

#endif //PSRDADA_CPP_PSRDADA_TO_SIGPROC_HEADER_HPP
