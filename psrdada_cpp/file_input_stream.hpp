#ifndef PSRDADA_CPP_FILE_INPUT_STREAM_HPP
#define PSRDADA_CPP_FILE_INPUT_STREAM_HPP
#include <fstream>
#include <cstdlib>
#include <vector>
#include "psrdada_cpp/raw_bytes.hpp"


/**
 * @details A simple file input stream. Will go through one entire file.
 * Will assume there is some amount of header to the file.
 */
namespace psrdada_cpp
{

template <class HandlerType>
class FileInputStream
{
public:
    FileInputStream(std::string filename, std::size_t headersize, std::size_t nbytes, HandlerType& handler)
    : _headersize(headersize), _nbytes(nbytes), _handler(handler)
    {
        _filestream.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        _filestream.open(filename.c_str(), std::ifstream::in | std::ifstream::binary);
        if (!_filestream.is_open())
        {
            throw std::runtime_error("File could not be opened");
        }
    }

    ~FileInputStream()
    {
        _filestream.close();
    }


    void start()
    {
        if (_running)
        {
            throw std::runtime_error("Stream is already running");
        }
        _running = true;

        // Get the header
        std::vector<char> header(4096);
        std::vector<char> data(_nbytes);
        char* header_ptr = header.data();
        char* data_ptr = data.data();
        RawBytes header_block(header_ptr, 4096, 0, false);
        _filestream.read(header_ptr, _headersize);
        header_block.used_bytes(4096);
        _handler.init(header_block);

        // Continue to stream data until the end
        while (!_stop)
        {
            BOOST_LOG_TRIVIAL(info) << "Reading data from the file";
            // Read data from file here
            RawBytes data_block(data_ptr, _nbytes, 0, false);
            while (!_stop)
            {
                if (_filestream.eof())
                {
                    BOOST_LOG_TRIVIAL(info) << "Reached end of file";
                    _filestream.close();
                    break;
                }
                _filestream.read(data_ptr, _nbytes);
                data_block.used_bytes(data_block.total_bytes());
                _handler(data_block);
            }
            data_block.~RawBytes();
        }
        _running = false;
    }

    void stop()
    {
        _stop = true;
    }


private:
    std::size_t _headersize;
    std::size_t _nbytes;
    std::ifstream _filestream;
    HandlerType& _handler;
    bool _stop = false;
    bool _running = false;
};

} //psrdada_cpp

#endif //PSRDADA_CPP_FILE_INPUT_STREAM_HPP
