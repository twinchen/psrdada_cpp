#include "psrdada_cpp/multilog.hpp"
#include "psrdada_cpp/raw_bytes.hpp"
#include "psrdada_cpp/dada_input_stream.hpp"
#include "psrdada_cpp/dada_disk_sink_leap.hpp"
#include "psrdada_cpp/cli_utils.hpp"
#include "psrdada_cpp/dada_write_client.hpp"
#include "boost/program_options.hpp"

#include <sys/types.h>
#include <iostream>
#include <string>
#include <sstream>
#include <ios>
#include <algorithm>

using namespace psrdada_cpp;

namespace
{
const size_t ERROR_IN_COMMAND_LINE = 1;
const size_t SUCCESS = 0;
const size_t ERROR_UNHANDLED_EXCEPTION = 2;
} // namespace

int main(int argc, char** argv)
{
    try
    {
        key_t key;
        std::string prefix;
        int nchan;
        /** Define and parse the program options
            */
        namespace po = boost::program_options;
        po::options_description desc("Options");
        desc.add_options()
        ("help,h", "Print help messages")
        ("key,k", po::value<std::string>()
         ->default_value("dada")
         ->notifier([&key](std::string in)
        {
            key = string_to_key(in);
        }),
        "The shared memory key for the dada buffer to connect to (hex string)")
        ("prefix,p", po::value<std::string>(&prefix)
         ->default_value("dbdisk_dump"),
         "Prefix for the filename to write to")
        ("nchan,n", po::value<int>(&nchan)
         ->default_value(8),
         "number of channels to split")
        ("log_level", po::value<std::string>()
         ->default_value("info")
         ->notifier([](std::string level)
        {
            set_log_level(level);
        }),
        "The logging level to use (debug, info, warning, error)");
        po::variables_map vm;
        try
        {
            po::store(po::parse_command_line(argc, argv, desc), vm);
            if ( vm.count("help")  )
            {
                std::cout << "DbDiskLeap -- read from DADA ring buffer and write to disk in LEAP spec" << std::endl
                          << desc << std::endl;
                return SUCCESS;
            }
            po::notify(vm);
        }
        catch (po::error& e)
        {
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
            std::cerr << desc << std::endl;
            return ERROR_IN_COMMAND_LINE;
        }

        /**
         * All the application code goes here
         */
        MultiLog log("dbdixk");
        DiskSinkLeap sink(prefix, nchan);
        DadaInputStream<decltype(sink)> stream(key, log, sink);
        stream.start();
        /**
         * End of application code
         */
    }
    catch (std::exception& e)
    {
        std::cerr << "Unhandled Exception reached the top of main: "
                  << e.what() << ", application will now exit" << std::endl;
        return ERROR_UNHANDLED_EXCEPTION;
    }
    return SUCCESS;

}
