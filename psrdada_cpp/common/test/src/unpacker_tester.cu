#include "psrdada_cpp/common/test/unpacker_tester.cuh"

#define BSWAP64(x) ((0xFF00000000000000 & x) >> 56) | \
                   ((0x00FF000000000000 & x) >> 40) | \
                   ((0x0000FF0000000000 & x) >> 24) | \
                   ((0x000000FF00000000 & x) >>  8) | \
                   ((0x00000000FF000000 & x) <<  8) | \
                   ((0x0000000000FF0000 & x) << 24) | \
                   ((0x000000000000FF00 & x) << 40) | \
                   ((0x00000000000000FF & x) << 56)

namespace psrdada_cpp {
namespace test {

UnpackerTester::UnpackerTester()
    : ::testing::Test()
    , _stream(0)
{

}

UnpackerTester::~UnpackerTester()
{

}

void UnpackerTester::SetUp()
{
    CUDA_ERROR_CHECK(cudaStreamCreate(&_stream));
}

void UnpackerTester::TearDown()
{
    CUDA_ERROR_CHECK(cudaStreamDestroy(_stream));
}

void UnpackerTester::unpacker_12_to_32_c_reference(
    InputType const& input,
    OutputType& output)
{
    uint64_t val, rest;
    assert(input.size() % 3 == 0 /*Input must be a multiple of 3 for 12-bit unpacking*/);
    output.reserve(input.size() * 64 / 12);
    for (std::size_t ii = 0; ii < input.size(); ii += 3)
    {
        val = be64toh(input[ii]);
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0xFFF0000000000000 & val) <<  0) >> 52));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x000FFF0000000000 & val) << 12) >> 52));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x000000FFF0000000 & val) << 24) >> 52));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x000000000FFF0000 & val) << 36) >> 52));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x000000000000FFF0 & val) << 48) >> 52));
        rest = ( 0x000000000000000F & val) << 60; // 4 bits rest.
        val = be64toh(input[ii + 1]);
        output.push_back(static_cast<float>(
            static_cast<int64_t>(((0xFF00000000000000 & val) >>  4) | rest) >> 52));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x00FFF00000000000 & val) <<  8) >> 52));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x00000FFF00000000 & val) << 20) >> 52));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x00000000FFF00000 & val) << 32) >> 52));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x00000000000FFF00 & val) << 44) >> 52));
        rest = ( 0x00000000000000FF & val) << 56; // 8 bits rest.
        val = be64toh(input[ii + 2]);
        output.push_back(static_cast<float>(
            static_cast<int64_t>(((0xF000000000000000 & val) >>  8) | rest) >> 52));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x0FFF000000000000 & val) <<  4) >> 52));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x0000FFF000000000 & val) << 16) >> 52));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x0000000FFF000000 & val) << 28) >> 52));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x0000000000FFF000 & val) << 40) >> 52));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x0000000000000FFF & val) << 52) >> 52));
    }
    assert(output.size() == input.size() * 64 / 12 /*Output is not the expected size*/);
}

void UnpackerTester::unpacker_10_to_32_c_reference(
    InputType const& input,
    OutputType& output)
{
    uint64_t val, rest;
    assert(input.size() % 5 == 0 /*Input must be a multiple of 5 for 10-bit unpacking*/);
    output.reserve(input.size() * 64 / 10);
    for (std::size_t ii = 0; ii < input.size(); ii += 5)
    {
        val = be64toh(input[ii]);
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0xFFC0000000000000 & val) <<  0) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x003FF00000000000 & val) << 10) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x00000FFC00000000 & val) << 20) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x00000003FF000000 & val) << 30) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x0000000000FFC000 & val) << 40) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x0000000000003FF0 & val) << 50) >> 54));
        rest =                   ( 0x000000000000000F & val) << 60; // 4 bits rest.
        val = be64toh(input[ii + 1]);
        output.push_back(static_cast<float>(
            static_cast<int64_t>(((0xFC00000000000000 & val) >> 4) | rest) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x03FF000000000000 & val) <<  6) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x0000FFC000000000 & val) << 16) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x0000003FF0000000 & val) << 26) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x000000000FFC0000 & val) << 36) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x000000000003FF00 & val) << 46) >> 54));
        rest = ( 0x00000000000000FF & val) << 56; // 8 bits rest.
        val = be64toh(input[ii + 2]);
        output.push_back(static_cast<float>(
            static_cast<int64_t>(((0xC000000000000000 & val) >> 8) | rest) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x3FF0000000000000 & val) <<  2) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x000FFC0000000000 & val) << 12) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x000003FF00000000 & val) << 22) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x00000000FFC00000 & val) << 32) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x00000000003FF000 & val) << 42) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x0000000000000FFC & val) << 52) >> 54));
        rest = ( 0x0000000000000003 & val) << 62; // 2 bits rest.
        val = be64toh(input[ii + 3]);
        output.push_back(static_cast<float>(
            static_cast<int64_t>(((0xFF00000000000000 & val) >> 2) | rest) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x00FFC00000000000 & val) <<  8) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x00003FF000000000 & val) << 18) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x0000000FFC000000 & val) << 28) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x0000000003FF0000 & val) << 38) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x000000000000FFC0 & val) << 48) >> 54));
        rest = ( 0x000000000000003F & val) << 58; // 6 bits rest.
        val = be64toh(input[ii + 4]);
        output.push_back(static_cast<float>(
            static_cast<int64_t>(((0xF000000000000000 & val) >> 6) | rest) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x0FFC000000000000 & val) <<  4) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x0003FF0000000000 & val) << 14) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x000000FFC0000000 & val) << 24) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x000000003FF00000 & val) << 34) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x00000000000FFC00 & val) << 44) >> 54));
        output.push_back(static_cast<float>(
            static_cast<int64_t>(( 0x00000000000003FF & val) << 54) >> 54));
    }
}

void UnpackerTester::unpacker_8_to_32_c_reference(
    InputType const& input,
    OutputType& output)
{
    output.reserve(input.size() * 8);
    for (uint64_t val: input)
    {
        val = be64toh(val);
        output.push_back(static_cast<float>(
            static_cast<int64_t>((0xFF00000000000000 & val) <<  0) >> 56));
        output.push_back(static_cast<float>(
            static_cast<int64_t>((0x00FF000000000000 & val) <<  8) >> 56));
        output.push_back(static_cast<float>(
            static_cast<int64_t>((0x0000FF0000000000 & val) << 16) >> 56));
        output.push_back(static_cast<float>(
            static_cast<int64_t>((0x000000FF00000000 & val) << 24) >> 56));
        output.push_back(static_cast<float>(
            static_cast<int64_t>((0x00000000FF000000 & val) << 32) >> 56));
        output.push_back(static_cast<float>(
            static_cast<int64_t>((0x0000000000FF0000 & val) << 40) >> 56));
        output.push_back(static_cast<float>(
            static_cast<int64_t>((0x000000000000FF00 & val) << 48) >> 56));
        output.push_back(static_cast<float>(
            static_cast<int64_t>((0x00000000000000FF & val) << 56) >> 56));
    }
}

void UnpackerTester::compare_against_host(
    Unpacker::OutputType const& gpu_output,
    OutputType const& host_output)
{
    OutputType copy_from_gpu = gpu_output;
    cudaDeviceSynchronize();
    ASSERT_EQ(host_output.size(), copy_from_gpu.size());
    for (std::size_t ii = 0; ii < host_output.size(); ++ii)
    {
	    // ASSERT_TRUE(host_output[ii] == copy_from_gpu[ii]) << " ii = " << ii;
	    EXPECT_EQ(host_output[ii], copy_from_gpu[ii]) << " ii = " << ii;
    }
}


TEST_F(UnpackerTester, test_8bit_unpacking_with_type_casting)
{
    std::size_t nsamples = 8192;
    thrust::host_vector<int8_t> vec = testing_tools::random_vector<int8_t>(0, 32, nsamples);
    thrust::host_vector<float> reference = testing_tools::conversion<int8_t, float>(vec);
    // reinterpret the data as uint64_t
    thrust::host_vector<uint64_t> hidata = thrust::host_vector<uint64_t>(
        reinterpret_cast<uint64_t*>(thrust::raw_pointer_cast(vec.data())),
        reinterpret_cast<uint64_t*>(thrust::raw_pointer_cast(vec.data())) + nsamples / 8
    );
    thrust::device_vector<uint64_t> idata = hidata;
    thrust::device_vector<float> odata(nsamples);

    Unpacker unpacker(_stream);
    unpacker.unpack<8>(idata, odata);
    Unpacker::OutputType h_odata = odata;
    compare_against_host(h_odata, reference);
}

TEST_F(UnpackerTester, test_8bit_unpacking_with_type_casting_non_512_multiple)
{
    std::size_t nsamples = 9320; // must be divisible by 8 as we generate int8_t data
    thrust::host_vector<int8_t> vec = testing_tools::random_vector<int8_t>(0, 32, nsamples);
    thrust::host_vector<float> reference = testing_tools::conversion<int8_t, float>(vec);
    // reinterpret the data as uint64_t
    thrust::host_vector<uint64_t> hidata = thrust::host_vector<uint64_t>(
        reinterpret_cast<uint64_t*>(thrust::raw_pointer_cast(vec.data())),
        reinterpret_cast<uint64_t*>(thrust::raw_pointer_cast(vec.data())) + nsamples / 8
    );
    thrust::device_vector<uint64_t> idata = hidata;
    thrust::device_vector<float> odata(nsamples);

    Unpacker unpacker(_stream);
    unpacker.unpack<8>(idata, odata);
    Unpacker::OutputType h_odata = odata;
    compare_against_host(h_odata, reference);
}

TEST_F(UnpackerTester, 12_bit_unpack_test)
{
    std::size_t n = 1024 * 3;
    InputType host_input = testing_tools::random_vector<uint64_t>(1, 1<<31, n);
    Unpacker::InputType gpu_input = host_input;
    Unpacker::OutputType gpu_output;
    gpu_output.resize(host_input.size() * sizeof(host_input[0]) * 8 / 12);
    OutputType host_output;
    Unpacker unpacker(_stream);
    unpacker.unpack<12>(gpu_input, gpu_output);
    unpacker_12_to_32_c_reference(host_input, host_output);
    compare_against_host(gpu_output, host_output);
}

TEST_F(UnpackerTester, 12_bit_unpack_test_non_512_multiple)
{
    std::size_t n = 1017*3;
    InputType host_input = testing_tools::random_vector<uint64_t>(1, 1<<31, n);
    Unpacker::InputType gpu_input = host_input;
    Unpacker::OutputType gpu_output;
    gpu_output.resize(host_input.size() * sizeof(host_input[0]) * 8 / 12);
    OutputType host_output;
    Unpacker unpacker(_stream);
    unpacker.unpack<12>(gpu_input, gpu_output);
    unpacker_12_to_32_c_reference(host_input, host_output);
    compare_against_host(gpu_output, host_output);
}

// TEST_F(UnpackerTester, 12_bit_unpack_test_bad_size)
// {
//     std::size_t n = 1000;
//     Unpacker::InputType gpu_input(n);
//     Unpacker::OutputType gpu_output(n);
//     Unpacker unpacker(_stream);
//     EXPECT_DEATH(unpacker.unpack<12>(gpu_input, gpu_output), "");
// }

TEST_F(UnpackerTester, 8_bit_unpack_test)
{
    std::size_t n = 512 * 8;
    InputType host_input = testing_tools::random_vector<uint64_t>(1, 1<<31, n);
    Unpacker::InputType gpu_input = host_input;
    Unpacker::OutputType gpu_output;
    gpu_output.resize(host_input.size() * sizeof(host_input[0]) * 8 / 8);
    OutputType host_output;
    Unpacker unpacker(_stream);
    unpacker.unpack<8>(gpu_input, gpu_output);
    unpacker_8_to_32_c_reference(host_input, host_output);
    compare_against_host(gpu_output, host_output);
}

TEST_F(UnpackerTester, 8_bit_unpack_test_non_512_multiple)
{
    std::size_t n = 511 * 8;
    InputType host_input = testing_tools::random_vector<uint64_t>(1, 1<<31, n);
    Unpacker::InputType gpu_input = host_input;
    Unpacker::OutputType gpu_output;
    gpu_output.resize(host_input.size() * sizeof(host_input[0]) * 8 / 8);
    OutputType host_output;
    Unpacker unpacker(_stream);
    unpacker.unpack<8>(gpu_input, gpu_output);
    unpacker_8_to_32_c_reference(host_input, host_output);
    compare_against_host(gpu_output, host_output);
}

// TEST_F(UnpackerTester, 10_bit_unpack_test_bad_size)
// {
//     std::size_t n = 641;
//     Unpacker::InputType gpu_input(n);
//     Unpacker::OutputType gpu_output(n);
//     Unpacker unpacker(_stream);
//     EXPECT_DEATH(unpacker.unpack<10>(gpu_input, gpu_output), "");
// }

TEST_F(UnpackerTester, 10_bit_unpack_test)
{
    std::size_t n = 640;
    InputType host_input = testing_tools::random_vector<uint64_t>(1, 1<<31, n);
    Unpacker::InputType gpu_input = host_input;
    Unpacker::OutputType gpu_output;
    gpu_output.resize(host_input.size() * sizeof(host_input[0]) * 8 / 10);
    OutputType host_output;
    Unpacker unpacker(_stream);
    unpacker.unpack<10>(gpu_input, gpu_output);
    unpacker_10_to_32_c_reference(host_input, host_output);
    compare_against_host(gpu_output, host_output);
}


} //namespace test
} //namespace psrdada_cpp
