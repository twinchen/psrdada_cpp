#include "psrdada_cpp/dada_disk_sink_multithread.hpp"

using namespace std;
using namespace std::chrono;

namespace psrdada_cpp {


DiskSinkMultithread::DiskSinkMultithread(std::string prefix, int nthread)
  : _prefix(prefix)
  , _counter(0)
  , _output_streams(nthread)
  , _nthread(nthread)
{
}
DiskSinkMultithread::~DiskSinkMultithread()
{
}
void DiskSinkMultithread::init(RawBytes& block)
{
    for (auto& of : _output_streams) {
        if (of.is_open()) {
            of.close();
        }
    }
    std::memcpy(&_header, block.ptr(), block.used_bytes());
    ascii_header_get(_header, "UTC_START", "%s", _start_time);
    BOOST_LOG_TRIVIAL(debug) << "UTC_START = " << _start_time;
    char buffer[1024];
    ascii_header_get(block.ptr(), "SAMPLE_CLOCK_START", "%s", buffer);
    std::size_t sample_clock_start = std::strtoul(buffer, NULL, 0);
    ascii_header_get(block.ptr(), "CLOCK_SAMPLE", "%s", buffer);
    long double sample_clock = std::strtold(buffer, NULL);
    ascii_header_get(block.ptr(), "SYNC_TIME", "%s", buffer);
    long double sync_time = std::strtold(buffer, NULL);
    long double unix_time = sync_time + (sample_clock_start / sample_clock);
    long double mjd_time = (unix_time / 86400.0 ) + 40587;
    std::ostringstream mjd_start;
    mjd_start << std::fixed;
    mjd_start << std::setprecision(12);
    mjd_start << mjd_time;
    ascii_header_set(_header, "MJD_START", "%s", mjd_start.str().c_str());
    ascii_header_set(_header, "UNIX_TIME", "%Lf", unix_time);
}
bool DiskSinkMultithread::operator()(RawBytes& block)
{
    for (auto& of : _output_streams) {
        if (of.is_open()) {
            of.close();
        }
    }
    std::string time;
    std::stringstream xx;
    xx << _start_time;
    time = xx.str();
    #pragma omp parallel for num_threads(_nthread)
    for (std::size_t ii = 0; ii < _nthread; ++ii)
    {
        std::size_t index = ii * block.used_bytes()/_nthread;
        char _loop_header[HEADER_SIZE];
        std::memcpy(&_loop_header, &_header, HEADER_SIZE);
        ascii_header_set(_loop_header, "OBS_OFFSET", "%ld", _counter + ii * block.used_bytes() / _nthread);
        ascii_header_set(_loop_header, "FILE_SIZE", "%ld", block.used_bytes() / _nthread);
        std::stringstream fname;
        fname <<  time.substr(11, 8) << "_" << std::setw(20) << std::setfill('0') << _counter + ii * block.used_bytes() / _nthread << ".dada";
        BOOST_LOG_TRIVIAL(debug) << "filename" << fname.str();
        _output_streams[ii].open(fname.str().c_str(), std::ios::out | std::ios::app | std::ios::binary);
        _output_streams[ii].write((char*) _loop_header, HEADER_SIZE);
        _output_streams[ii].write(block.ptr()+index, block.used_bytes()/_nthread);
    }
    _counter +=  block.used_bytes();
    return false;
}


} //namespace psrdada_cpp
